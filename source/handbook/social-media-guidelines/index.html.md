---
layout: markdown_page
title: "Social Media Guidelines"
---

Go to the [Handbook](/handbook/)

## Responding to Requests

Any time GitLab is mentioned or relevant to a thread, someone from GitLab should be responding to questions or feedback. The idea here is to be available for people without making them feel obligated to talk to you. Remain positive.

- **Continue the conversation by providing meaningful responses instead of just upvoting or saying a one word “Thanks!”** If a post will add to a discussion, post it. If someone has given you feedback, link them to a relevant issue showing the feedback has been received. You can even provide people with links to non-GitLab things such as Git guides or DevOps guides. There is always a feature to mention or gratitude to express 'Thanks you so much for spreading the word about GitLab.'.
- **Remember that you’re speaking to a human being.** The people behind the comments are real, so treat them how you’d want to be treated if the roles were reversed. Try to find their real name so that you can personalize your message to them.
- **Begin by thanking them for their feedback or apologizing to them for any inconvenience they may have experienced.** We want to start and end conversations on a high note.

- **Assume good faith**. Remember, people are commenting on the product, not the people. The line can get blurry, but it’s important to stay objective.
- **Address any and all points the user has made.** If they made 4 points/requests, respond to each. If you don’t know the answer, don’t be afraid to tell them you don’t know but will look into it.
- **You’re allowed to disagree with people.** Try to inform the person (respectfully) why they might be misguided and provide any links they may need to make a more informed decision. Don’t say “you’re wrong” or “that’s stupid.” Instead try to say “I disagree because…” or “I don’t think that’s accurate because…”
- **Replies are not endorsements.** Just because you’re replying and not publicly disagreeing doesn’t mean you agree with the statement.
- **Always seek feedback.** If someone has something negative to say, ask them how we could make it better. If they can provide examples that’s even better.
- **Open issues!** If someone has a point they’d like to discuss, feel free to open an issue and link them to it. Regardless of whether or not you agree with the point, you should be inviting the community to participate in GitLab’s direction. Be sure to link to the relevant post in the issue for easier tracking. This won’t work for all cases, so use your best judgement.
- **Do what you say you will.** If you say you'll look into something, set a reminder to circle back with the person or link them to an issue on the issue tracker. Saying you'll look into something makes it impossible to continue the conversation until you do so, so it is essential that you continue the conversation at some later point. If this is too much work just say 'I don't know' and leave it at that.

## GitLab Voice

You are personally responsible for the tweets, likes and replies you post on social media while representing GitLab. Everything you publish is publicly viewable and will be available for a long time even if redacted. Be careful and thoughtful when using the company accounts.

When speaking for GitLab, use the “GitLab voice.” When replying from the official GitLab account, speak as “we” and represent the software and community. On the official @GitLab account Twitter account and in other social media we should model attributes of our software and community. We strive to respond to all messages and questions. We respond by encouraging collaboration and contribution.

Consider what benefits the software and community, and how the software would respond if we personified it. Be responsive, positive, open minded, curious, welcoming, apologetic, transparent, direct, and honest. Someone doesn’t like something? Ask them to tell us more in the issue tracker. Someone thinks GitLab could be better? Invite them to submit a feature proposal. Any criticism is an opportunity to improve our software.

When responding to posts from your personal account, feel free to incorporate your own style and voice. Talk to people as if you were talking to them in person.

## Dealing with Conflict

You may come across angry users from time to time. When dealing with people who are confrontational, it’s important to remain level-headed. You may also send them to Sid directly.

- Assume good faith. People have opinions and sometimes they’re strong ones. It’s usually not personal.
- If it’s getting personal, step away from the conversation and delegate to someone else.
- Sometimes all people need is acknowledgement. Saying “Sorry things aren’t working for you” can go a long way.

This document is a guide for GitLab team members who manage social media accounts. 

You are personally responsible for the tweets, likes and replies you post on social media while representing GitLab. Everything you publish is publicly viewable and will be available for a long time even if redacted. Be careful and thoughtful when using the company accounts. 