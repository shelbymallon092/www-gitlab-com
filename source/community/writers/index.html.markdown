---
layout: markdown_page
title: "Write for GitLab"
comments: false
sharing: true
suppress_header: true
---
![Write for GitLab](/images/community/computers-table-banner.jpg)

# Write for GitLab

We’ve opened [our blog] up to contributions from the community.

When you get published on our blog you can earn $50 to $200 for technical articles. If you’re accepted, you’ll get feedback on your writing, and be guided through making the best resources. Find out how to get started!

All great contributions come from developers “scratching their own itch.” It’s likely you can share some advice to help someone along the way. You can contribute the technical content you wish was available.

## How it works

1.  Choose a topic and submit a writing sample to us
2.  Get approved
3.  Start writing, get feedback and revise
4.  Publish
5.  Get paid

First you’ll submit a writing sample and tell us about your areas of expertise. After you get approved, we’ll contact you to initiate the writing process. We will review in-depth your post, and you’ll get detailed feedback on your writing before we push the article to publication. Once it’s published you’ll get paid.

In terms of pricing, we looked to the industry to see what were the going rates. Your feedback is welcome. Here are our rough guidelines for rates:

- Brief "Quick tips" or feature highlights of less than 800 words. - USD 50.00
- Short tutorials of 800-1500 words. - USD 100.00
- In-depth tutorials or opinion pieces of 1,500+ words. - USD 200.00

## What we're looking for

We’re inviting community contribution so we can expand the range of tutorials and advice about creating, collaborating and deploying with GitLab.

It's important that the content is:

- Accurate
- Complete
- Original

## Topics

To find out what topics we're looking for, review [the blog post backlog][topics-issues] and see if there are any existing requests for topics that inspire you. That can help you identify our most high-priority topics. However, if the subject you want to write about is not there, feel free to add a new issue with it, explainig why do you think that is important for the GitLab Community.

### Example topic areas

- Comparison posts, e.g., Git v W; GitLab v X; GitLab CI v Y; GitLab EE v Z
- Migrating from X to Git and/or GitLab
- Working with GitLab, feature highlights and tutorials
- Ways of boosting efficiency
- Extending capability with integrations
- Improving communication in code-collaboration
- Managing open source projects
- Testing X with GitLab CI
- Case studies
- How-tos

## Writing process

We have some stantards, so please, make sure you've **read them before submitting your proposal**. The technical process of writing and reviewing can be found in our Handbooks: [GitLab Blog] and [Technical Writing].

## Get Paid

When your post gets published, it's time to claim for your compensation. You will send us an invoice, GitLab will pay you in American Dollars (USD) from a bank account in the USA, via wired transfer to your bank account. We can deal with other possible payment methods via credit card, but please discuss it with us before start writing.

_**Note:** When you start writing for GitLab, we assume you have accepted the terms described along this page._

[topics-issues]: https://gitlab.com/gitlab-com/blog-posts/issues?milestone_id=&scope=all&sort=created_desc&state=opened&utf8=%E2%9C%93&assignee_id=0&author_id=&milestone_title=&label_name=&weight=
[our blog]: https://about.gitlab.com/blog/
[GitLab Blog]: https://about.gitlab.com/handbook/marketing/product-marketing/content-marketing/#blog
[Technical Writing]: https://about.gitlab.com/handbook/marketing/developer-relations/technical-writing/#professional-writing-techniques
